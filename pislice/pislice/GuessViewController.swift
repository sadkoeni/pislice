//
//  GuessViewController.swift
//  pislice
//
//  Created by Ali Attar on 1/27/18.
//  Copyright © 2018 Ali Attar. All rights reserved.
//

import UIKit

class GuessViewController: UIViewController {
    @IBOutlet weak var digit0: UILabel!
    @IBOutlet weak var digit1: UILabel!
    @IBOutlet weak var digit2: UILabel!
    @IBOutlet weak var digit3: UILabel!
    @IBOutlet weak var digit4: UILabel!
    @IBOutlet weak var digit5: UILabel!
    @IBOutlet weak var index: UILabel!
    
    let digitsList = [1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9, 3]
    var index_num = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        digit3.layer.masksToBounds = true
        digit3.layer.cornerRadius = digit3.frame.width / 2
        for subview in self.view.subviews {
            for subsubview in subview.subviews{
                if subsubview is UIButton{
                    subsubview.layer.masksToBounds = true
                    subsubview.layer.cornerRadius = digit3.frame.width / 2
                }
            }
        }
        resetGame()
    }
    
    @IBAction func buttonTap(_ sender: UIButton) {
        if sender.titleLabel?.text == "\(digitsList[index_num])" {
            correctGuess()
        }
        else {
            resetGame()
        }
    }
    
    func correctGuess() {
        digit3.text = "\(digitsList[index_num])"
        
        DispatchQueue.main.asyncAfter(deadline: .now()
            + .milliseconds(200), execute: {
                self.digit3.text = "?"
                self.digit0.text = self.digit1.text
                self.digit1.text = self.digit2.text
                self.digit2.text = "\(self.digitsList[self.index_num])"
                self.index_num += 1
                self.index.text = "\(self.index_num)"
        })
    }
    
    func resetGame() {
        index_num = 0
        index.text = "\(index_num)"
        digit0.text = " "
        digit1.text = "3"
        digit2.text = "."
        digit3.text = "?"
        digit4.text = "?"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}


